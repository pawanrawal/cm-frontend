# CM Frontend

This project allows interacting with the Campaign Monitor API through a proxy server written in Go.
It allows fetching lists for a client, adding a new list with a single subscriber.

1. Run the proxy server (it runs on 8080 by default)

The proxy server is important as direct interaction with Campaign monitor API is not possible because
of CORS.

```
./proxy_server/proxy_server -apikey <key> -client_id <client_id>
```

2. Run `npm install` from the root of the repo to install dependencies for the frontend.

3. Run `npm start`. This should start serving frontend on port `3000`.