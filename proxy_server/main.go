package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

var (
	apiKey   = flag.String("apikey", "", "API key to do authentication.")
	clientId = flag.String("client_id", "", "Campaign monitor client id.")
	port     = flag.String("port", "8080", "Port to run the server on.")
)

const baseUrl = "https://api.createsend.com/api/v3.1"

func makeRequest(url string, body []byte) (*http.Response, error) {
	req, err := http.NewRequest("POST", url, bytes.NewReader(body))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		return nil, err
	}

	req.SetBasicAuth(*apiKey, "")
	client := &http.Client{}
	resp, err := client.Do(req)
	return resp, err
}

func addSubscriber(listId string, email string, w http.ResponseWriter) (*http.Response, error) {
	p := map[string]string{"EmailAddress": email}
	b, err := json.Marshal(p)
	if err != nil {
		return nil, err
	}

	return makeRequest(fmt.Sprintf("%s/subscribers/%s.json", baseUrl, listId), b)
}

type Params struct {
	Email string
	List  json.RawMessage
}

// Errors from CM backend API have a Message field in them, so we keep our structure the same.
func errorMessage(msg string) []byte {
	return []byte(fmt.Sprintf(`{"Message": "%s"}`, msg))
}

func addList(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var p Params
	json.Unmarshal(reqBody, &p)

	// Create list.
	resp, err := makeRequest(fmt.Sprintf("%s/lists/%s.json", baseUrl, *clientId), p.List)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(errorMessage("While creating list."))
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil || resp.StatusCode != http.StatusCreated {
		w.WriteHeader(resp.StatusCode)
		w.Write(body)
		return
	}

	var listId string
	err = json.Unmarshal(body, &listId)
	if listId == "" || err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(errorMessage("While creating list."))
		return
	}

	if p.Email == "" {
		return
	}

	resp, err = addSubscriber(listId, p.Email, w)
	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil || resp.StatusCode != http.StatusCreated {
		w.WriteHeader(resp.StatusCode)
		w.Write(body)
		return
	}
	w.Write(body)
	return
}

func listsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	if r.Method == http.MethodPost {
		addList(w, r)
		return
	}

	req, err := http.NewRequest("GET", fmt.Sprintf("%s/clients/%s/lists.json", baseUrl, *clientId), nil)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	req.SetBasicAuth(*apiKey, "")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil || resp.StatusCode != http.StatusOK {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(body)
		return
	}
	w.Write(body)
}

func main() {
	flag.Parse()

	if *clientId == "" || *apiKey == "" {
		fmt.Println("client_id and apikey flags can't be empty.")
		os.Exit(1)
	}

	http.HandleFunc("/lists", listsHandler)
	fmt.Println("Listening on port:" + *port)
	log.Fatal(http.ListenAndServe(":"+*port, nil))
}
