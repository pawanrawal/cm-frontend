import React, { Component } from "react";
import "./App.css";

import { Modal, Button, Navbar, Table, Alert } from "react-bootstrap";

const BASE_URL = "http://localhost:8080/lists";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lists: [],
      isFetching: "true",
      error: "",
      showModal: false,

      title: "",
      unsubscribePage: "",
      confirmationPage: "",
      unsubscribeSetting: "AllClientLists",
      confirmedOptIn: false,
      email: "",

      success: "",
      alertVisible: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleAlertDismiss = this.handleAlertDismiss.bind(this);
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true });
  }

  fetchLists() {
    var that = this;
    fetch(BASE_URL, {
      method: "GET"
    })
      .then(function(response) {
        that.setState({
          isFetching: false
        });

        if (!response.ok) {
          return response.json();
        }

        return response.json();
      })
      .then(data => {
        if (data.Message !== undefined) {
          this.setState({
            alertVisible: true,
            error: "Internal server error: " + data.Message
          });
          return;
        }

        this.setState({ lists: data });
      })
      .catch(error => {
        this.setState({
          alertVisible: true,
          error: "Internal server error: " + error
        });
      });
  }

  handleAlertDismiss() {
    this.setState({ alertVisible: false });
  }

  componentDidMount() {
    this.fetchLists();
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({
      error: "",
      success: ""
    });

    var req = {
      List: {
        Title: this.state.title,
        UnsubscribePage: this.state.unsubscribePage,
        UnsubscribeSetting: this.state.unsubscribeSetting,
        ConfirmedOptIn: this.state.confirmedOptIn === "true",
        ConfirmationSuccessPage: this.state.confirmationPage
      },
      Email: this.state.email
    };
    var that = this;
    fetch(BASE_URL, {
      method: "POST",
      body: JSON.stringify(req)
    })
      .then(function(response) {
        if (response.status !== 201) {
          // Hide Modal
          that.setState({
            showModal: false
          });
          return response.json();
        }

        that.fetchLists();
        that.setState({
          showModal: false
        });
        return null;
      })
      .then(data => {
        if (data.Message !== undefined) {
          this.setState({
            alertVisible: true,
            error: "Internal server error: " + data.Message
          });
          return;
        }
        this.setState({
          alertVisible: true,
          success: "List with subscriber added successfully."
        });
      })
      .catch(error => {
        this.setState({
          alertVisible: true,
          error: "Internal server error: " + error
        });
      });
  }

  render() {
    var isFetching = this.state.isFetching;
    return (
      <div>
        <Navbar inverse collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <a>CM Frontend</a>
            </Navbar.Brand>
          </Navbar.Header>
        </Navbar>

        <div style={{ marginTop: "10px" }}>
          {isFetching ? <h3>Loading...</h3> : ""}
        </div>

        {this.state.alertVisible && this.state.error !== "" ? (
          <Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
            {this.state.error}
          </Alert>
        ) : (
          ""
        )}

        {this.state.alertVisible && this.state.success !== "" ? (
          <Alert bsStyle="success" onDismiss={this.handleAlertDismiss}>
            {this.state.success}
          </Alert>
        ) : (
          ""
        )}

        <div className="text-right" style={{ marginRight: "20px" }}>
          <Button onClick={this.open} bsStyle="success">
            Add List
          </Button>
        </div>

        <div className="col-md-8 col-md-offset-2">
          <h3> Lists</h3>

          {this.state.lists.length > 0 ? (
            <Table striped bordered condensed hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                </tr>
              </thead>
              <tbody>
                {this.state.lists.map((list, idx) => (
                  <tr key={list.ListID}>
                    <th scope="row">{idx + 1}.</th>
                    <td>{list.Name}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            ""
          )}
        </div>

        <Modal show={this.state.showModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label className="col-form-label">Title:</label>
                <input
                  type="text"
                  className="form-control"
                  value={this.state.title}
                  name="title"
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label className="col-form-label">Unsubscribe Page:</label>
                <input
                  type="text"
                  className="form-control"
                  value={this.state.unsubscribePage}
                  name="unsubscribePage"
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-group">
                <label className="col-form-label">Unsubscribe setting:</label>
                <select
                  className="custom-select form-control"
                  value={this.state.unsubscribeSetting}
                  name="unsubscribeSetting"
                  onChange={this.handleChange}
                >
                  <option value="AllClientLists">All Client Lists</option>
                  <option value="OnlyThisList">Only This List</option>
                </select>
              </div>

              <div className="form-group">
                <label className="col-form-label">Confirmed Opt In:</label>
                <select
                  className="custom-select form-control"
                  value={this.state.confirmedOptIn}
                  name="confirmedOptIn"
                  onChange={this.handleChange}
                >
                  <option value={false}>False</option>
                  <option value={true}>True</option>
                </select>
              </div>
              <div className="form-group">
                <label className="col-form-label">
                  Confirmation Success Page:
                </label>
                <input
                  type="text"
                  className="form-control"
                  value={this.state.confirmationPage}
                  name="confirmationPage"
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-group">
                <label className="col-form-label">Subscriber Email:</label>
                <input
                  type="text"
                  className="form-control"
                  value={this.state.email}
                  name="email"
                  onChange={this.handleChange}
                />
              </div>
            </form>{" "}
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close}>Close</Button>
            <Button onClick={this.handleSubmit}>Add</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default App;
